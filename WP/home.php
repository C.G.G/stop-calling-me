<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>
<body>
  <div class="background"></div>
  <div class="overlay"></div>
  <div class="container-fluid home">
    <div class="row">
<div class="col-sm-12 nav-bar">
<a class="fa fa-bars"></a>
      <nav>
        <?php
        $page = get_post(96);
        echo "$page->post_content";
        ?>
      </nav>
    </div>
      <div class="col-sm-12 intro">
        <h1 class="text-xs-center company-name"><?php bloginfo('name'); ?></h1>
        <div class="main-block">
          <h1 class="text-xs-center main-text"><?php bloginfo('description'); ?></h1>
          <p class="text-xs-center lower-text">Recover up to $500 to $1500 per unsolicited call or text.</p>
          <div class="button-wrapper center-block">
            <a href="https://glapionlaw.typeform.com/to/UBW8l0"><button type="button" class="btn btn-secondary-outline btn-lg btn-left">Do you have a case?</button></a>
            <a href="/faq"><button type="button" class="btn btn-secondary-outline btn-lg btn-right">How does it work?</button></a>
          </div>
        </div>
  <div class="odometer">
<ul class="digits">
<li class="digit dollar d1">
  <span class="cou-item">
    <ul>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
      <li>$</li>
    </ul>
  </span>
</li>
<li class="digit d2">
  <span class="cou-item">
    <ul>
      <li>0</li>
      <li></li>
      <li>1 ,</li>
      <li>2 ,</li>
      <li>3 ,</li>
      <li>4 ,</li>
      <li>5 ,</li>
      <li>6 ,</li>
      <li>7 ,</li>
      <li>8 ,</li>
      <li>9 ,</li>
      <li>0 ,</li>
    </ul>
  </span>
</li>
<li class="digit d3">
  <span class="cou-item">
    <ul>
      <li>0</li>
      <li>0</li>
      <li>1</li>
      <li>2</li>
      <li>3</li>
      <li>4</li>
      <li>5</li>
      <li>6</li>
      <li>7</li>
      <li>8</li>
      <li>9</li>
      <li>0</li>
    </ul>
  </span>
</li>
<li class="digit d4">
  <span class="cou-item">
    <ul>
      <li>0</li>
      <li>0</li>
      <li>1</li>
      <li>2</li>
      <li>3</li>
      <li>4</li>
      <li>5</li>
      <li>6</li>
      <li>7</li>
      <li>8</li>
      <li>9</li>
      <li>0</li>
    </ul>
  </span>
</li>
<li class="digit d5">
  <span class="cou-item">
    <ul>
      <li>0</li>
      <li>0 ,</li>
      <li>1 ,</li>
      <li>2 ,</li>
      <li>3 ,</li>
      <li>4 ,</li>
      <li>5 ,</li>
      <li>6 ,</li>
      <li>7 ,</li>
      <li>8 ,</li>
      <li>9 ,</li>
      <li>0 ,</li>
    </ul>
  </span></li>
<li class="digit d6">
  <span class="cou-item">
    <ul>
      <li>0</li>
      <li>0</li>
      <li>1</li>
      <li>2</li>
      <li>3</li>
      <li>4</li>
      <li>5</li>
      <li>6</li>
      <li>7</li>
      <li>8</li>
      <li>9</li>
      <li>0</li>
    </ul>
  </span></li>
<li class="digit d7">
  <span class="cou-item">
    <ul>
      <li>0</li>
      <li>0</li>
      <li>1</li>
      <li>2</li>
      <li>3</li>
      <li>4</li>
      <li>5</li>
      <li>6</li>
      <li>7</li>
      <li>8</li>
      <li>9</li>
      <li>0</li>
    </ul>
  </span></li>
<li class="digit d8">
  <span class="cou-item">
    <ul>
      <li>0</li>
      <li>0</li>
      <li>1</li>
      <li>2</li>
      <li>3</li>
      <li>4</li>
      <li>5</li>
      <li>6</li>
      <li>7</li>
      <li>8</li>
      <li>9</li>
      <li>0</li>
    </ul>
  </span></li>
</ul>
<br><br><br>
<p class="text-xs-center lower-text counter-description">RECOVERED SINCE...</p>
</div>
      </div>
    </div>
  </div>
  <?php get_footer('home'); ?>
