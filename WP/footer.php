  <?php wp_footer(); ?>
  <div class="pre-footer"></div>
  <footer>
    <p class="text-xs-center footer-nav" style="color:white;"><?php
        $page = get_post(102);
        echo "$page->post_content";
        ?></p>
    <p class="text-xs-center"><?php
        $page2 = get_post(117);
        echo "$page2->post_content";
        ?></p>
  </footer>
  <!-- jQuery first, then Bootstrap JS. -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Flowtype.js/1.1.0/flowtype.min.js"></script>
  <script>
  $('body').flowtype({
     minimum   : 500,
     maximum   : 1200,
     minFont   : 12,
     maxFont   : 15,
     fontRatio : 30
  });
  </script>
</body>

</html>
