  <?php wp_footer(); ?>
  <!-- jQuery first, then Bootstrap JS. -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Flowtype.js/1.1.0/flowtype.min.js"></script>
  <script>
  $('body').flowtype({
     minimum   : 500,
     maximum   : 1200,
     minFont   : 12,
     maxFont   : 15,
     fontRatio : 30
  });
  </script>
<script>
  function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
  }
  var moneyAmount = <?php $post = get_post( 65 ); $counterValue = $post->post_content; echo "$counterValue" ?>;
  var moneyDigits = moneyAmount.toString().length+1;
  if(moneyDigits<8) {
    $('.d2').css('display', 'none');
  }
  $(window).on('load resize', function(){
  var counterWidth = $('.odometer').outerWidth();
  var digitWidth = $('.digit').outerWidth();
  var digitsWidth = digitWidth * moneyDigits;
  var halfDigitsWidth = digitsWidth /2;
  $('.odometer').css('padding-left', ((counterWidth/2) - halfDigitsWidth) + 'px');
});
var num = 10000000 + moneyAmount;
  setTimeout(function(){
    $('.cou-item').find('ul').each(function(i, el){
      var val = pad(num, 5, 0).split("");
      var $el = $(this);
      $el.removeClass();
      $el.addClass('goto-' + val[i]);
    })
  }, 2000);
  </script>
<script>
  $(document).ready(function(){
    $(".fa-bars").click(function(e) {
      $('.nav-bar').toggleClass( "expanded");
      $("nav").toggleClass( "expandedNav");
      $(".company-name").toggleClass( "hidden");
    });


  });
  </script>
</body>

</html>
