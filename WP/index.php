<?php
/*
Template Name: Generic Page
*/
?>
<?php get_header(); ?>
<body>
  <div class="container-fluid faq">
    <div class="row">
      <div class="col-sm-12 nav-bar">
        <h1 class="title">Stop Calling Me -
          <br>Glapion Law Firm</h1>
        <nav>
	<?php
        $page = get_post(96);
        echo "$page->post_content";
        ?>
        </nav>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 content">
	<?php
        $postId = url_to_postid( $url );
        $page = get_post($postId);
        echo "$page->post_content";
        ?>
      </div>
    </div>
  </div>
 <?php get_footer(); ?>
