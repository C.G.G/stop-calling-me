function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
var moneyAmount = 1215000;
var num = 10000000 + moneyAmount;
setTimeout(function(){
  $('.cou-item').find('ul').each(function(i, el){
    var val = pad(num, 5, 0).split("");
    var $el = $(this);
    $el.removeClass();
    $el.addClass('goto-' + val[i]);
  })
}, 1000);
